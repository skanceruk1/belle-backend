from typing import Any

from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.conf import settings
from django.http import HttpRequest


class AccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request: HttpRequest):
        return getattr(settings, "ACCOUNT_ALLOW_REGISTRATION", True)


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def is_open_for_signup(self, request: HttpRequest, sociallogin: Any):
        return getattr(settings, "ACCOUNT_ALLOW_REGISTRATION", True)


class BelleAccountAdapter(DefaultAccountAdapter):

    def is_open_for_signup(self, request: HttpRequest):
        return getattr(settings, "ACCOUNT_ALLOW_REGISTRATION", True)

    def save_user(self, request, user, form, commit=False):
        user = super().save_user(request, user, form, commit)
        data = form.cleaned_data
        user.name = data.get('name')
        user.phone_number = data.get('phone_number')
        user.number_of_children = data.get('number_of_children')
        user.college_start_date = data.get('college_start_date')
        user.plaid_public_token = data.get('plaid_public_token')
        user.plaid_access_token = data.get('plaid_access_token')
        user.plaid_credit_card_ID = data.get('plaid_credit_card_ID')
        user.plaid_bank_account_ID = data.get('plaid_bank_account_ID')
        user.plaid_credit_card_item_ID = data.get('plaid_credit_card_item_ID')
        user.plaid_bank_account_item_ID = data.get('plaid_bank_account_item_ID')
        user.offers_accepted_comma_seperated_list = data.get('offers_accepted_comma_seperated_list')
        user.plaid_access_token_bank = data.get('plaid_access_token_bank')
        user.profile = data.get('profile')
        user.test_user = data.get('test_user')
        user.save()
        return user