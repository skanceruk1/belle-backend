# users/serializers.py
from rest_framework import serializers
from rest_auth.registration.serializers import RegisterSerializer
from .models import BelleUser, BelleUserProfile
from rest_auth.models import TokenModel

class BelleUserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = BelleUserProfile
        fields = () #'phone_number', 'number_of_children', 'college_start_date')


# class BelleUserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = models.BelleUser
#         fields = ('username', 'email', 'phone_number', 'number_of_children', 'college_start_date')


# class BelleUserSerializer(serializers.HyperlinkedModelSerializer):
#     profile = BelleUserProfileSerializer(required=True)
#
#     class Meta:
#         model = BelleUser
#         fields = ('url', 'email', 'first_name', 'last_name', 'password', 'profile')
#         extra_kwargs = {'password': {'write_only': True}}
#
#     # def create(self, validated_data):
#     #     profile_data = validated_data.pop('profile')
#     #     password = validated_data.pop('password')
#     #     user = BelleUser(**validated_data)
#     #     user.set_password(password)
#     #     user.save()
#     #     BelleUserProfile.objects.create(user=user, **profile_data)
#     #     return user
#
#     def update(self, instance, validated_data):
#         profile_data = validated_data.pop('profile')
#         profile = instance.profile
#
#         instance.email = validated_data.get('email', instance.email)
#         instance.save()
#
#         profile.title = profile_data.get('title', profile.title)
#         profile.dob = profile_data.get('dob', profile.dob)
#         profile.address = profile_data.get('address', profile.address)
#         profile.country = profile_data.get('country', profile.country)
#         profile.city = profile_data.get('city', profile.city)
#         profile.zip = profile_data.get('zip', profile.zip)
#         profile.photo = profile_data.get('photo', profile.photo)
#         profile.save()
#
#         return instance


class BelleUserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = BelleUser
        fields = ('url', 'username', 'email', 'name', 'number_of_children', 'college_start_date', 'plaid_public_token', 'plaid_public_token',
                  'plaid_access_token', 'plaid_access_token', 'plaid_credit_card_ID', 'plaid_bank_account_ID',
                  'plaid_credit_card_item_ID', 'plaid_bank_account_item_ID', 'offers_accepted_comma_seperated_list',
                  'plaid_credit_card_ID_comma_seperated_list', 'plaid_bank_account_ID_comma_seperated_list',
                  'plaid_access_token_bank', 'test_user')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = BelleUser(**validated_data)
        user.set_password(password)

        user.save()
        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile')
        profile = instance.profile

        instance.email = validated_data.get('email', instance.email)
        instance.number_of_children = validated_data.get('number_of_children', instance.email)
        instance.college_start_date = validated_data.get('college_start_date', instance.email)
        instance.plaid_public_token = validated_data.get('plaid_public_token', instance.plaid_public_token)
        instance.save()

        # profile.phone_number = profile_data.get('phone_number', profile.phone_number)
        # profile.number_of_children = profile_data.get('number_of_children', profile.number_of_children)
        # profile.college_start_date = profile_data.get('college_start_date', profile.college_start_date)

        profile.save()

        return instance


class BelleUserRegisterSerializer(RegisterSerializer):

    email = serializers.EmailField(required=True)
    password1 = serializers.CharField(write_only=True)
    name = serializers.CharField(required=True)

    phone_number = serializers.IntegerField(required=True)
    number_of_children = serializers.IntegerField(required=True)
    college_start_date = serializers.DateField(required=True)

    plaid_public_token = serializers.CharField(required=True)
    plaid_access_token = serializers.CharField(required=False)
    plaid_credit_card_ID = serializers.CharField(required=False)
    plaid_bank_account_ID = serializers.CharField(required=False)
    plaid_credit_card_item_ID = serializers.CharField(required=False)
    plaid_bank_account_item_ID = serializers.CharField(required=False)
    offers_accepted_comma_seperated_list = serializers.CharField(required=False)
    plaid_credit_card_ID_comma_seperated_list = serializers.CharField(required=False)
    plaid_bank_account_ID_comma_seperated_list = serializers.CharField(required=False)
    plaid_access_token_bank = serializers.CharField(required=False)
    test_user = serializers.BooleanField(required=False)
    def get_cleaned_data(self):
        super(BelleUserRegisterSerializer, self).get_cleaned_data()

        return {
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'name': self.validated_data.get('name', ''),
            'username': self.validated_data.get('username', ''),
            'phone_number': self.validated_data.get('phone_number', ''),
            'number_of_children': self.validated_data.get('number_of_children', ''),
            'college_start_date': self.validated_data.get('college_start_date', ''),
            'plaid_public_token': self.validated_data.get('plaid_public_token', ''),
            'plaid_access_token': self.validated_data.get('plaid_access_token', ''),
            'plaid_credit_card_ID': self.validated_data.get('plaid_credit_card_ID', ''),
            'plaid_bank_account_ID': self.validated_data.get('plaid_bank_account_ID', ''),
            'plaid_credit_card_item_ID': self.validated_data.get('plaid_credit_card_item_ID', ''),
            'plaid_bank_account_item_ID': self.validated_data.get('plaid_bank_account_item_ID', ''),
            'offers_accepted_comma_seperated_list': self.validated_data.get('offers_accepted_comma_seperated_list', ''),
            'plaid_credit_card_ID_comma_seperated_list': self.validated_data.get('plaid_credit_card_ID_comma_seperated_list', ''),
            'plaid_bank_account_ID_comma_seperated_list': self.validated_data.get('plaid_bank_account_ID_comma_seperated_list', ''),
            'plaid_access_token_bank': self.validated_data.get('plaid_access_token_bank', ''),
            'test_user': self.validated_data.get('test_user', '')
        }


class BelleUserDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = BelleUser
        fields = ('username', 'email', 'name', 'phone_number', 'number_of_children', 'college_start_date',
                  'plaid_public_token', 'plaid_access_token', 'plaid_credit_card_ID', 'plaid_bank_account_ID',
                  'plaid_credit_card_item_ID', 'plaid_bank_account_item_ID', 'offers_accepted_comma_seperated_list',
                  'plaid_access_token_bank', 'test_user')
        # read_only_fields = ('email',)


class TokenSerializer(serializers.ModelSerializer):
    """
    Serializer for Token model.
    """
    user = BelleUserDetailsSerializer(many=False, read_only=True)  # this is add by myself.
    class Meta:
        model = TokenModel
        fields = ('key', 'user')   # there I add the `user` field ( this is my need data ).