from django.contrib.auth import get_user_model, forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from .models import BelleUser

# User = get_user_model()


class BelleUserChangeForm(forms.UserChangeForm):
    class Meta(forms.UserChangeForm.Meta):
        model = BelleUser
        fields = forms.UserChangeForm.Meta.fields


class BelleUserCreationForm(forms.UserCreationForm):

    error_message = forms.UserCreationForm.error_messages.update(
        {"duplicate_username": _("This username has already been taken.")}
    )

    class Meta(forms.UserCreationForm.Meta):
        model = BelleUser
        fields = ('username', 'email') #, 'phone_number', 'number_of_children', 'college_start_date')

    def clean_username(self):
        username = self.cleaned_data["username"]

        try:
            BelleUser.objects.get(username=username)
        except BelleUser.DoesNotExist:
            return username

        raise ValidationError(self.error_messages["duplicate_username"])
