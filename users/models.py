from django.contrib.auth.models import AbstractUser
from django.db.models import CharField, PositiveIntegerField, DateField, BooleanField
# from django.db.models.functions import datetime
import datetime
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.db import models
from django.conf import settings


class BelleUser(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.


    # date_of_birth =  DateField(default=datetime.date.today)
    name = models.CharField(_("Name of User"), blank=True, max_length=255)

    username = models.CharField(blank=True, null=True, max_length=255)
    email = models.EmailField(_('email address'), unique=True, max_length=255)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    # phone_number = models.PositiveIntegerField(_('phone_number'), default=0)
    # number_of_children = models.PositiveIntegerField(_('number_of_children'), default=0)
    # college_start_date = models.DateField(_('college_start_date'), default=timezone.now)

    phone_number = PositiveIntegerField(_('phone_number'), default=0)
    number_of_children = PositiveIntegerField(_('Number of children'), default=0)
    college_start_date = DateField(_('College start date'), default=timezone.now)

    plaid_public_token = CharField(_('plaid_public_token'), max_length=150, blank=True)
    plaid_access_token = CharField(_('plaid_access_token'), max_length=150, blank=True)
    plaid_access_token_bank = CharField(_('plaid_access_token_bank'), max_length=150, blank=True)

    plaid_credit_card_ID = CharField(_('plaid_credit_card_ID'), max_length=150, blank=True)
    plaid_bank_account_ID = CharField(_('plaid_bank_account_ID'), max_length=150, blank=True)

    plaid_credit_card_item_ID = CharField(_('plaid_credit_card_item_ID'), max_length=150, blank=True)
    plaid_bank_account_item_ID = CharField(_('plaid_bank_account_item_ID'), max_length=150, blank=True)

    offers_accepted_comma_seperated_list = CharField(_('offers_accepted_comma_seperated_list'), max_length=150, blank=True)

    test_user = BooleanField(_('test_user'), default=False)

    ## just for test purposes not used yet
    plaid_credit_card_ID_comma_seperated_list = CharField(_('plaid_credit_card_ID_comma_seperated_list'), max_length=150, blank=True)
    plaid_bank_account_ID_comma_seperated_list = CharField(_('plaid_bank_account_ID_comma_seperated_list'), max_length=150, blank=True)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})



class BelleUserProfile(models.Model):

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')

    phone_number = models.PositiveIntegerField(_('Phone Number'), default=0)
    number_of_children = models.PositiveIntegerField(_('Number of children'), default=0)
    college_start_date = models.DateField(_('College start date'), default=timezone.now)