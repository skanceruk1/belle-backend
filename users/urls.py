from django.urls import include, path
from django.conf.urls import url

from . import views
from rest_framework import routers
from .views import BelleUserViewSet

# from belle.users.views import (
#     user_list_view,
#     user_redirect_view,
#     user_update_view,
#     user_detail_view,
# )

router = routers.DefaultRouter()
router.register("users", BelleUserViewSet)

# app_name = "users"
urlpatterns = [
    # path("", view=views.UserListView.as_view(), name="list"),
    # path("~redirect/", view=views.UserRedirectView, name="redirect"),
    # path("~update/", view=views.UserUpdateView, name="update"),
    # path("<str:username>/", view=views.UserDetailView, name="detail"),
    path("", include(router.urls)),
    path("auth/", include("rest_auth.urls")),
    # url(r'^', include(router.urls)),
    # path('users/', include('belle.users.urls')),
    # path('rest-auth/', include('rest_auth.urls')),
]
