# Generated by Django 2.0.13 on 2019-04-21 03:53

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_belleuserprofile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='belleuserprofile',
            name='address',
        ),
        migrations.RemoveField(
            model_name='belleuserprofile',
            name='city',
        ),
        migrations.RemoveField(
            model_name='belleuserprofile',
            name='country',
        ),
        migrations.RemoveField(
            model_name='belleuserprofile',
            name='dob',
        ),
        migrations.RemoveField(
            model_name='belleuserprofile',
            name='photo',
        ),
        migrations.RemoveField(
            model_name='belleuserprofile',
            name='title',
        ),
        migrations.RemoveField(
            model_name='belleuserprofile',
            name='zip',
        ),
        migrations.AddField(
            model_name='belleuserprofile',
            name='college_start_date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='college_start_date'),
        ),
        migrations.AddField(
            model_name='belleuserprofile',
            name='name',
            field=models.CharField(blank=True, max_length=255, verbose_name='Name of User'),
        ),
        migrations.AddField(
            model_name='belleuserprofile',
            name='number_of_children',
            field=models.PositiveIntegerField(default=0, verbose_name='number_of_children'),
        ),
        migrations.AddField(
            model_name='belleuserprofile',
            name='phone_number',
            field=models.PositiveIntegerField(default=0, verbose_name='phone_number'),
        ),
    ]
