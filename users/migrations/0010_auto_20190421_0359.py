# Generated by Django 2.0.13 on 2019-04-21 03:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_auto_20190421_0353'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='belleuser',
            name='college_start_date',
        ),
        migrations.RemoveField(
            model_name='belleuser',
            name='number_of_children',
        ),
        migrations.RemoveField(
            model_name='belleuser',
            name='phone_number',
        ),
        migrations.RemoveField(
            model_name='belleuserprofile',
            name='name',
        ),
    ]
