# Generated by Django 2.0.13 on 2019-04-30 00:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0016_auto_20190430_0010'),
    ]

    operations = [
        migrations.AddField(
            model_name='belleuser',
            name='offers_accepted_comma_seperated_list',
            field=models.CharField(blank=True, max_length=150, verbose_name='offers_accepted_comma_seperated_list'),
        ),
        migrations.AlterField(
            model_name='belleuser',
            name='plaid_bank_account_ID',
            field=models.CharField(blank=True, max_length=150, verbose_name='plaid_bank_account_ID'),
        ),
    ]
