# Generated by Django 2.0.13 on 2019-04-21 01:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20190421_0058'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='belleuser',
            name='plaid_Access_Token',
        ),
        migrations.RemoveField(
            model_name='belleuser',
            name='plaid_BankAccount_ID',
        ),
        migrations.RemoveField(
            model_name='belleuser',
            name='plaid_Credit_Card_ID',
        ),
        migrations.RemoveField(
            model_name='belleuser',
            name='plaid_Public_Token',
        ),
        migrations.AddField(
            model_name='belleuser',
            name='plaid_access_token',
            field=models.CharField(blank=True, max_length=150, verbose_name='plaid_access_token'),
        ),
        migrations.AddField(
            model_name='belleuser',
            name='plaid_bank_account_ID',
            field=models.CharField(blank=True, max_length=150, verbose_name='plaid_bank_account_ID'),
        ),
        migrations.AddField(
            model_name='belleuser',
            name='plaid_credit_card_ID',
            field=models.CharField(blank=True, max_length=150, verbose_name='plaid_credit_card_ID'),
        ),
        migrations.AddField(
            model_name='belleuser',
            name='plaid_public_token',
            field=models.CharField(blank=True, max_length=150, verbose_name='plaid_public_token'),
        ),
        migrations.AlterField(
            model_name='belleuser',
            name='number_of_children',
            field=models.PositiveIntegerField(default=0, verbose_name='number_of_children'),
        ),
    ]
