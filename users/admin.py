from django.contrib import admin
from django.contrib.auth import admin as auth_admin

from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

# from belle.users.\
from .forms import BelleUserChangeForm, BelleUserCreationForm
from .models import BelleUser, BelleUserProfile


class BelleUserProfileInline(admin.StackedInline):
    model = BelleUserProfile
    can_delete = False


@admin.register(BelleUser)
class BelleUserAdmin(BaseUserAdmin):
    # form = BelleUserChangeForm
    # add_form = BelleUserCreationForm
    fieldsets = (("User", {"fields": ("name",)}),) + auth_admin.UserAdmin.fieldsets

    # fieldsets = (
    #     (None, {'fields': ('email', 'password')}),
    #     (_('Personal info'), {'fields': ('name')}),
    #     (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    # )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ("username", "name", "email", "is_superuser") ##, "test_user")
    # , 'phone_number', "number_of_children", "college_start_date", "plaid_public_token"]
    search_fields = ('email',"name")
    ordering = ('email',)
    inlines = (BelleUserProfileInline,)


# admin.site.register(BelleUser, BelleUserAdmin)