from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from .models import BelleUser
from .serializers import BelleUserSerializer
from rest_framework import viewsets

from rest_framework import generics

from . import models
from . import serializers

# User = BelleUser


class BelleUserViewSet(viewsets.ModelViewSet):
    queryset = BelleUser.objects.all()
    serializer_class = BelleUserSerializer


class UserDetailView(LoginRequiredMixin, DetailView):

    model = BelleUser
    slug_field = "username"
    slug_url_kwarg = "username"


user_detail_view = UserDetailView.as_view()


class UserListView(LoginRequiredMixin, ListView):

    model = BelleUser
    slug_field = "username"
    slug_url_kwarg = "username"
    queryset = models.BelleUser.objects.all()
    serializer_class = serializers.BelleUserSerializer


user_list_view = UserListView.as_view()


class UserUpdateView(LoginRequiredMixin, UpdateView):

    model = BelleUser
    fields = ["name"]

    def get_success_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})

    def get_object(self):
        return BelleUser.objects.get(username=self.request.user.username)


user_update_view = UserUpdateView.as_view()


class UserRedirectView(LoginRequiredMixin, RedirectView):

    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})


user_redirect_view = UserRedirectView.as_view()
